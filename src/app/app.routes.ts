import { Routes } from '@angular/router';

export const routes: Routes = [
    {path: '', redirectTo: '/conductor', pathMatch: 'full'},
    {path: 'conductor', loadComponent:() => import('./pages/conductor/conductor.component').then(m => m.ConductorComponent)},
    {path: 'bus', loadComponent:() => import('./pages/bus/bus.component').then(m => m.BusComponent)}
];
