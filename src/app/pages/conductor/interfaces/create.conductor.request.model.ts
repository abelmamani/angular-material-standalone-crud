export interface CreateConductorRequestModel {
    dni: string,
    name: string,
    lastName: string,
    hireDate: Date
}