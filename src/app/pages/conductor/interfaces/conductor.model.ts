export interface Conductor {
    id: number,
    dni: string,
    name: string,
    lastName: string,
    hireDate: Date
}