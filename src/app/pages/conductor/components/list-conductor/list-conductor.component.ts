import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Conductor } from '../../interfaces/conductor.model';
import { ConductorService } from '../../services/conductor.service';
import { CreateConductorComponent } from '../create-conductor/create-conductor.component';
import { DeleteConductorComponent } from '../delete-conductor/delete-conductor.component';
import { UpdateConductorComponent } from '../update-conductor/update-conductor.component';
@Component({
  selector: 'app-list-conductor',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButtonModule, MatSnackBarModule, MatDialogModule],
  templateUrl: './list-conductor.component.html',
  styleUrl: './list-conductor.component.scss'
})
export class ListConductorComponent implements OnInit{
  conductors: Conductor[] = [];
  constructor(private conductorService: ConductorService, private snackbar: MatSnackBar, private dialog: MatDialog){}
  ngOnInit(): void {
    this.getAllConductors();
  }

  getAllConductors(){
    this.conductorService.getAllConductors().subscribe({
      next: (res: Conductor[]) => {
        this.conductors = res;
      },
      error: (err) => {
        this.showSanckBar("Hubo un problema al obtener el listado de conductores");    
      }
    });
  }
  
  showSanckBar(msg: string){
    this.snackbar.open(msg, "Aceptar", {duration: 3000});  
  }

  openCreateDialog(){
    const dialogRef = this.dialog.open(CreateConductorComponent);
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getAllConductors();
        this.showSanckBar("se añadio nuevo conductor");
      }else{
        this.showSanckBar("accion cancelada");
      }
    });
  }

  openUpdateDialog(id: number){
    const dialogRef = this.dialog.open(UpdateConductorComponent);
    dialogRef.componentInstance.id = id;
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getAllConductors();
        this.showSanckBar("se actualizo conductor");
      }else{
        this.showSanckBar("accion cancelada");
      }
    });
  }

  openDeleteDialog(id: number){
    const dialogRef = this.dialog.open(DeleteConductorComponent);
    dialogRef.componentInstance.id = id;
    dialogRef.afterClosed().subscribe( result => {
      if(result){
        this.getAllConductors();
        this.showSanckBar("se elimino conductor");
      }else{
        this.showSanckBar("accion cancelada");
      }
    });
  }
}
