import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Conductor } from '../../interfaces/conductor.model';
import { ConductorService } from '../../services/conductor.service';
@Component({
  selector: 'app-update-conductor',
  standalone: true,
  providers: [provideNativeDateAdapter()],
  imports: [CommonModule, ReactiveFormsModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDatepickerModule],
  templateUrl: './update-conductor.component.html',
  styleUrl: './update-conductor.component.scss'
})
export class UpdateConductorComponent implements OnInit{
  @Input() id!: number;
  conductorForm: FormGroup = this.formBuilder.group({
    id: [0, Validators.required],
    dni: ['', Validators.required],
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    hireDate: [new Date(), Validators.required],
  });
  constructor(private conductorService: ConductorService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<UpdateConductorComponent>){}
  ngOnInit(): void {
    this.getConductorById(this.id);
  }
  getConductorById(id: number){
    this.conductorService.getConductorByid(id).subscribe({
      next: (res: Conductor) => {
        this.conductorForm.patchValue(res);
      },
      error: (err) => {
        this.showSnackBar("Hubo un problema al obtner el conductor");
      }
    });
  }

  onSubmit(){
    if(this.conductorForm.valid){
      this.conductorService.updateConductor(this.conductorForm.value).subscribe({
        next: (res: any) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnackBar(err.error.message ? err.error.message : "Hubo un problema al actualizar conductor");
        }
      });
    }else{
      this.conductorForm.touched;
    }
  }
  showSnackBar(msg: string){
    this.snackBar.open(msg, "Aceptar", {duration: 3000});
  }
}
