import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConductorService } from '../../services/conductor.service';

@Component({
  selector: 'app-create-conductor',
  standalone: true,
  providers: [provideNativeDateAdapter()],
  imports: [CommonModule, ReactiveFormsModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule, MatDatepickerModule],
  templateUrl: './create-conductor.component.html',
  styleUrl: './create-conductor.component.scss'
})
export class CreateConductorComponent {
  conductorForm: FormGroup = this.formBuilder.group({
    dni: ['', Validators.required],
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    hireDate: [new Date(), Validators.required],
  });
  constructor(private conductorService: ConductorService, private formBuilder: FormBuilder, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<CreateConductorComponent>){}
  onSubmit(){
    if(this.conductorForm.valid){
      this.conductorService.createConductor(this.conductorForm.value).subscribe({
        next: (res: any) => {
          this.dialogRef.close(true);
        },
        error: (err) => {
          this.showSnackBar(err.error.message ? err.error.message : "Hubo un problema al registrar conductor");
        }
      });
    }else{
      this.conductorForm.touched;
    }
  }
  showSnackBar(msg: string){
    this.snackBar.open(msg, "Aceptar", {duration: 3000});
  }
}
