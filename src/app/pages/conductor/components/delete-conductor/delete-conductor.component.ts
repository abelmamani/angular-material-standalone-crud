import { Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { ConductorService } from '../../services/conductor.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'app-delete-conductor',
  standalone: true,
  imports: [MatCardModule, MatButtonModule, MatSnackBarModule],
  templateUrl: './delete-conductor.component.html',
  styleUrl: './delete-conductor.component.scss'
})
export class DeleteConductorComponent {
  @Input() id!: number;
  constructor(private conductorService: ConductorService, private snackBar: MatSnackBar, private dialogRef: MatDialogRef<DeleteConductorComponent>){}
  delete(){
    this.conductorService.deleteConductor(this.id).subscribe({
      next: (res: any) => {
        this.dialogRef.close(true);
      },
      error: (err) => {
        this.showSnackBar(err.error.message ? err.error.message : 'Hubo un problema al eliminar el conductor');
      }
    });
  }
  cancel(){
    this.dialogRef.close(false);
  }
  showSnackBar(msg: string){
    this.snackBar.open(msg, 'Aceptar', {duration: 3000});
  }
}
