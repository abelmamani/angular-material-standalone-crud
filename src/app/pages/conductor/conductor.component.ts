import { Component } from '@angular/core';
import { ListConductorComponent } from './components/list-conductor/list-conductor.component';

@Component({
  selector: 'app-conductor',
  standalone: true,
  imports: [ListConductorComponent],
  templateUrl: './conductor.component.html',
  styleUrl: './conductor.component.scss'
})
export class ConductorComponent {

}
