import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateConductorRequestModel } from '../interfaces/create.conductor.request.model';
import { Observable } from 'rxjs';
import { Conductor } from '../interfaces/conductor.model';

@Injectable({
  providedIn: 'root'
})
export class ConductorService {
  url: string = "http://localhost:8080/conductors";
  constructor(private http: HttpClient) { }
  getAllConductors(): Observable<Conductor[]>{
    return this.http.get<Conductor[]>(this.url);
  }
  getConductorByid(id: number): Observable<Conductor>{
    return this.http.get<Conductor>(this.url + "/" + id);
  }
  createConductor(conductor: CreateConductorRequestModel): Observable<any>{
    return this.http.post(this.url, conductor);
  }
  
  updateConductor(conductor: Conductor): Observable<any>{
    return this.http.put(this.url, conductor);
  }

  deleteConductor(id: number): Observable<any>{
    return this.http.delete(this.url + "/" + id);
  }
}
